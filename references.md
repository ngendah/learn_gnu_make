References
============

1. GNU make book. http://www.gnu.org/software/make.
2. The linux Programmer's Toolbox by John Fusco.
3. Managing Projects with GNU make by Robert Mecklenburg.
4. C and C++ compiling by Milan Stevanovic.